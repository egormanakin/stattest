<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

class RunCommand extends Command
{
    protected static $defaultName = 'app:run';

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $fieldsCount = (int) $io->ask('Свободные ячейки');
        $chipCount = (int) $io->ask('Занятые ячейки');

        if ($chipCount > $fieldsCount) {
            $io->error('Неверно введены данные');
            return;
        }

//        $count = (int) gmp_strval(gmp_fact($fieldsCount)) / ((int) gmp_strval(gmp_fact($fieldsCount - $chipCount)) * (int) gmp_strval(gmp_fact($chipCount)));
//        var_dump($count);

        $arr = [];

        for ($field = 1; $field <= $fieldsCount; $field++) {
            $arr[] = $field <= $chipCount ? 1 : 0;
        }



        $vars = [];
        $vars[] = $arr;

        for ($c = 1; $c <= $chipCount; $c++) {
            $j = $chipCount;
            $k = $fieldsCount;

            $tempVars = [];
            $tempVars[] = $arr;

            while ($j > 0) {
                for ($i = $j; ($i < $k && $i >= $c); $i++) {
                    $var = $tempVars[sizeof($tempVars) - 1];

                    for ($a = 1; $a <= $c; $a++) {
                        $var[$i - $a] = 0;
                        $var[$i - $a + 1] = 1;
                    }

                    $tempVars[] = $var;
                }


                foreach ($tempVars as $tempVar) {
                    if (!in_array($tempVar, $vars)) {
                        $vars[] = $tempVar;
                    }
                }

                $k = $k - $c;
                $j = $j - $c;
            }
        }

        foreach ($vars as $var) {
            $reversed = array_reverse($var);

            if (!in_array($reversed, $vars)) {
                $vars[] = $reversed;
            }
        }

        foreach ($vars as $key => $value) {
            $vars[$key] = implode(' ', $value);
        }

        $varsCount = sizeof($vars);

        $io->section('Количество: ' . $varsCount);

        $out = '';
        foreach ($vars as $var) {
            $io->text($var);
            $out .= $var . PHP_EOL;
        }

        $fs = new Filesystem();

        $content = $varsCount >= 10 ? 'Количество: ' . $varsCount . PHP_EOL . $out : 'Меньше 10 записей';
        $fs->dumpFile('file.txt', $content);

        $io->success('Файл создан');
    }
}
